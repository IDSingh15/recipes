//
//  RecipeTableViewCell.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import UIKit

class RecipeTableViewCell: UITableViewCell {

    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.recipeImageView.layer.cornerRadius = 10
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.recipeImageView.image = nil
    }
}
