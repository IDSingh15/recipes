//
//  RecipesTableViewController.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import UIKit

class RecipesTableViewController: UITableViewController {
    
    var response: SpoonacularQueryResponseModel!
   
    var query: String?
    var cuisine: SpoonacularCuisine?
    var diet: SpoonacularDiet?
    
    var recipes: [SpoonacularResultModel]!
    
    private var images: [UIImage?]!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Recipes"
        self.recipes = self.response.results
    }
    
    private func loadMoreResults() {
        if self.recipes.count < self.response.totalResults {
            SpoonacularManager.shared.searchRecipes(withQuery: self.query, number: self.response.totalResults - self.recipes.count > 10 ? 10 : self.response.totalResults - self.recipes.count, offset: self.response.offset + 1, cuisine: self.cuisine, andDiet: self.diet) { result in
                switch result {
                case .success(let response):
                    self.response = response
                    self.recipes.append(contentsOf: response.results)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                case .failure(let error):
                    print(error.rawValue)
                }
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recipes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipe", for: indexPath) as! RecipeTableViewCell
        cell.recipeLabel.text = self.recipes[indexPath.row].title
        cell.recipeImageView.setImage(fromURL: URL(string: self.recipes[indexPath.row].image))
        
        if indexPath.row == self.recipes.count - 1 {
            self.loadMoreResults()
        }
        
        return cell
    }
}
