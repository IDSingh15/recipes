//
//  ViewController.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var keywordTextField: UITextField!
    @IBOutlet weak var cuisineTextField: UITextField!
    @IBOutlet weak var dietTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    private var cuisinePickerView: UIPickerView!
    private var dietPickerView: UIPickerView!
    
    private var selectedCuisine: SpoonacularCuisine?
    private var selectedDiet: SpoonacularDiet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Search Recipes"
        self.navigationItem.backButtonTitle = "Search"
        self.setupTextFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setupSearchButton()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.hideKeyboard()
    }
    
    private func hideKeyboard() {
        self.keywordTextField.resignFirstResponder()
        self.cuisineTextField.resignFirstResponder()
        self.dietTextField.resignFirstResponder()
    }
    
    private func setupTextFields() {
        self.setupPickerView(&self.cuisinePickerView)
        self.cuisineTextField.inputView = self.cuisinePickerView
        self.setupPickerView(&self.dietPickerView)
        self.dietTextField.inputView = self.dietPickerView
    }
    
    private func setupPickerView(_ pickerView: inout UIPickerView?) {
        pickerView = UIPickerView()
        pickerView!.delegate = self
        pickerView!.dataSource = self
    }
    
    private func setupSearchButton() {
        self.searchButton.layer.cornerRadius = 10
        self.searchButton.setTitleColor(.gray, for: .highlighted)
        self.searchButton.addTarget(self, action: #selector(self.searchButtonDidPress), for: .touchUpInside)
        self.searchButton.addTarget(self, action: #selector(self.searchButtonTouchBegan), for: .touchDown)
        self.searchButton.addTarget(self, action: #selector(self.searchButtonTouchEnded), for: .touchUpOutside)
    }
    
    @objc private func searchButtonDidPress() {
        self.searchButtonTouchEnded()
        if self.keywordTextField.text != "" || self.selectedCuisine != nil || self.selectedDiet != nil {
            self.performSearch()
        } else {
            let alert = UIAlertController(title: "Oops!", message: "Cannot perform an empty search. Please fill at least one field.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc private func searchButtonTouchBegan() {
        UIView.animate(withDuration: 0.1) {
            self.searchButton.transform = CGAffineTransform(scaleX: 0.98, y: 0.98)
        }
    }
    
    @objc private func searchButtonTouchEnded() {
        UIView.animate(withDuration: 0.1) {
            self.searchButton.transform = CGAffineTransform.identity
        }
    }
    
    private func performSearch() {
        self.searchButton.isEnabled = false
        SpoonacularManager.shared.searchRecipes(withQuery: self.keywordTextField.text, cuisine: self.selectedCuisine, andDiet: self.selectedDiet) { result in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    if let recipesVC = UIStoryboard(name: "Main", bundle:  .main).instantiateViewController(identifier: "recipes") as? RecipesTableViewController {
                        recipesVC.response = response
                        recipesVC.query = self.keywordTextField.text
                        recipesVC.cuisine = self.selectedCuisine
                        recipesVC.diet = self.selectedDiet
                        self.navigationController?.pushViewController(recipesVC, animated: true)
                    }
                }
            case .failure(let error):
                self.showAlert(withError: error)
            }
            DispatchQueue.main.async {
                self.searchButton.isEnabled = true
            }
        }
    }
    
    private func showAlert(withError error: SpoonacularError) {
        print("Error:", error.rawValue)
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Oops!", message: error.rawValue, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

//MARK: UIPickerViewDataSource

extension SearchViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerView == self.cuisinePickerView ? SpoonacularCuisine.allCases.count : SpoonacularDiet.allCases.count + 1
    }
}

//MARK: UIPickerViewDelegate

extension SearchViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "-"
        } else {
            return pickerView == self.cuisinePickerView ? SpoonacularCuisine.allCases[row - 1].rawValue.capitalized : SpoonacularDiet.allCases[row - 1].rawValue.capitalized
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.cuisinePickerView {
            self.selectedCuisine = row == 0 ? nil : SpoonacularCuisine.allCases[row - 1]
            self.cuisineTextField.text = self.selectedCuisine?.rawValue.capitalized ?? ""
        } else {
            self.selectedDiet = row == 0 ? nil : SpoonacularDiet.allCases[row - 1]
            self.dietTextField.text = self.selectedDiet?.rawValue.capitalized ?? ""
        }
    }
}
