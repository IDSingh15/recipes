//
//  SpoonacularCuisine.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import Foundation

enum SpoonacularCuisine: String, CaseIterable {
    
    case african
    case american
    case british
    case cajun
    case caribbean
    case chinese
    case easternEuropean    = "Eastern European"
    case european
    case french
    case german
    case greek
    case indian
    case irish
    case italian
    case japanese
    case jewish
    case korean
    case latinAmerican      = "Latin American"
    case mediterranean
    case middleEastern      = "Middle Eastern"
    case nordic
    case southern
    case spanish
    case thai
    case vietnamese
}
