//
//  SpoonacularDiet.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import Foundation

enum SpoonacularDiet: String, CaseIterable {
    
    case glutenFree         = "Gluten Free"
    case ketogenic
    case vegetarian
    case lactoVegetarian    = "Lacto-Vegetarian"
    case ovoVegetarian      = "Ovo-Vegetarian"
    case vegan
    case pescetarian
    case paleo
    case primal
    case whole30
}
