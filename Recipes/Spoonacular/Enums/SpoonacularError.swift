//
//  SpoonacularError.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import Foundation

enum SpoonacularError: String, Error {
    
    case invalidURL         = "URL is invalid."
    case noRecipesFound     = "No recipes found. Try searching with some other combination."
    case emptyResponseData  = "Received empty Data from the server."
    case badJSON            = "Bad JSON received in response."
    case otherError         = "Some other error occurred."
}
