//
//  SpoonacularQueryResponseModel.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import Foundation

struct SpoonacularQueryResponseModel: Decodable {
    
    var number: Int
    var offset: Int
    var totalResults: Int
    var results: [SpoonacularResultModel]
}
