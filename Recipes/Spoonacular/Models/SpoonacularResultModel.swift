//
//  SpoonacularResultModel.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import Foundation

struct SpoonacularResultModel: Decodable {
    
    var id: Int
    var image: String
    var imageType: String
    var title: String
}
