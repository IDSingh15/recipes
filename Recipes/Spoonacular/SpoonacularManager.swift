//
//  SpoonacularManager.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import Foundation

class SpoonacularManager {
    
    static let shared = SpoonacularManager()
    
    private var baseURLString = "https://api.spoonacular.com"
    private let apiKey = "561b6a035cae4b7eaed095ebc4c1952b"
    
    /// Function to search recipes from the Spoonacular API.
    /// - Parameters:
    ///   - query: String passed along by the user.
    ///   - number: Number of recipes required. Default value is 10.
    ///   - offset: Result page offset. Default value is 0.
    ///   - cuisine: Cuisine of the recipe(s)
    ///   - diet: Diet for which the recipe(s) must be suitable.
    ///   - completion: Returning a result to the caller.
    
    func searchRecipes(withQuery query: String?, number: Int = 10, offset: Int = 0, cuisine: SpoonacularCuisine?, andDiet diet: SpoonacularDiet?, onCompletion completion: @escaping (Result<SpoonacularQueryResponseModel, SpoonacularError>) -> Void) {

        var searchURLString = baseURLString + "/recipes/complexSearch?"
        
        if let query = query {
            searchURLString.append("query=\(query)&")
        }
        
        if let cuisine = cuisine {
            searchURLString.append("cuisine=\(cuisine.rawValue)&")
        }
        
        if let diet = diet {
            searchURLString.append("diet=\(diet.rawValue)&")
        }
        
        searchURLString.append("number=\(number)&")
        searchURLString.append("offset=\(offset)&")
        searchURLString.append("apiKey=\(self.apiKey)")
        
        searchURLString = searchURLString.replacingOccurrences(of: " ", with: "%20")

        self.performQuery(withURLString: searchURLString, onCompletion: completion)
    }
    
    /// Function to perform the query using URLRequest and URLSession.
    /// - Parameters:
    ///   - urlString: Formatted URL string for the GET Request.
    ///   - completion: Returning the result to the caller.
    
    private func performQuery(withURLString urlString: String, onCompletion completion: @escaping (Result<SpoonacularQueryResponseModel, SpoonacularError>) -> Void) {
        
        print("URL:", urlString)
        
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidURL))
            return
        }
        
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {
                
                guard let data = data else {
                    completion(.failure(.emptyResponseData))
                    return
                }
                
                guard let response = try? JSONDecoder().decode(SpoonacularQueryResponseModel.self, from: data) else {
                    completion(.failure(.badJSON))
                    return
                }
                
                print("Total Items:", response.totalResults)
                print("Requested Items:", response.number)
                print("Offset:", response.offset)
                for recipe in response.results {
                    print(recipe.title)
                }
                
                if response.results.count == 0 {
                    completion(.failure(.noRecipesFound))
                }
                
                completion(.success(response))
            } else {
                print(error!.localizedDescription)
                completion(.failure(.otherError))
            }
        }.resume()
    }
}
