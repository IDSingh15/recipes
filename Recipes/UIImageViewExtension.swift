//
//  UIImageViewExtension.swift
//  Recipes
//
//  Created by Inder Deep Singh on 05/03/21.
//

import UIKit

extension UIImageView {
    
    func setImage(fromURL url: URL?) {
        guard let url = url else { return }
        DispatchQueue.global().async { [weak self] in
            if let imageData = try? Data(contentsOf: url) {
                if let image = UIImage(data: imageData) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
